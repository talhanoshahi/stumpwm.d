#+TITLE: StumpWM Config files
#+AUTHOR: Muhammad Talha Noshahi
#+DESCRIPTION: This is the readme file for the repository for my dotfilescontaining my config files for StumpWM
#+STARTUP: overview
#+OPTIONS: toc:nil

Consult the init.org file if you wanna have some documentation regarding the init.lisp file.

* Tasks
- [X] Make an init.org file for StumpWM config.
- [-] Make the modeline clickable and make it similar to your dwm's status bar.
  - [X] Learn how to make a single module clickable.
  - [ ] Complete all modules.
  
