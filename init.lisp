(in-package :stumpwm)

(set-module-dir "~/.stumpwm.d/modules")

(setf *startup-message* nil)

(set-prefix-key (kbd "C-z"))

; some default variables to set
(setf *message-window-gravity* :center
      *input-window-gravity* :center
      *window-border-style :thin
      *message-window-padding* 10
      *maxsize-border-width* 2
      *normal-border-width* 2
      *transient-border-width* 2
      *float-window-border* 4
      *float-window-tile-height* 20
      *float-window-modifier* :SUPER
      *mouse-focus-policy* :sloppy)

(defvar *shell-command* "/bin/sh")

(defvar *bot/workspaces*
  (list
    "Web"
    "Editor"
    "Whatever 1"
    "Whatever 2"
    "Whatever 3"
    "Whatever 4"
    "Whatever 5"
    "Whatever 6"
    "Whatever 7"))

(defvar *move-to-workspace-keybinds* (list "!" "@" "#" "$" "%" "^" "&" "*" "("))

(stumpwm:grename (nth 0 *bot/workspaces*))
(dolist (workspace (cdr *bot/workspaces*))
  (stumpwm:gnewbg workspace))

(dotimes (y (length *bot/workspaces*))
  (let ((workspace (write-to-string (+ y 1))))
    (define-key *root-map* (kbd workspace) (concat "gselect " workspace))
    (define-key *root-map* (kbd (nth y *move-to-workspace-keybinds*)) (concat "gmove-and-follow " workspace))))

; keybinds
(define-key *root-map* (kbd "Q") "quit")
(define-key *root-map* (kbd "R") "restart-hard")

(define-key *root-map* (kbd "q") "delete")
(define-key *root-map* (kbd "r") "remove")

(define-key *root-map* (kbd "h") "move-focus left")
(define-key *root-map* (kbd "j") "move-focus down")
(define-key *root-map* (kbd "k") "move-focus up")
(define-key *root-map* (kbd "l") "move-focus right")
(define-key *root-map* (kbd "H") "move-window left")
(define-key *root-map* (kbd "J") "move-window down")
(define-key *root-map* (kbd "K") "move-window up")
(define-key *root-map* (kbd "L") "move-window right")
(define-key *root-map* (kbd "x") "fullscreen")

(setf *resize-increment* 50)
(define-key *top-map* (kbd "M-l") "resize-direction Right")
(define-key *top-map* (kbd "M-h") "resize-direction Left")
(define-key *top-map* (kbd "M-k") "resize-direction Up")
(define-key *top-map* (kbd "M-j") "resize-direction Down")

(define-key *root-map* (kbd "RET") "exec /usr/bin/kitty")
(define-key *root-map* (kbd "w") "exec /usr/bin/firefox")
(define-key *root-map* (kbd "g") "bot/toggle-modeline")

(load-module "cpu")
(load-module "battery-portable")

(setf *mode-line-timeout* 5)

(set-font "-*-hack nerd font mono-medium-r-*-*-17-120-100-100-*-*-*-*")

; *time-format* = 0 means only the time in the format 12:00 AM
; *time-format* = 1 means time along with the current date
(defparameter *time-format* 0)
(defun bot/modeline/date-and-time/time ()
  "Returns the current date of the system"
  (if (eq *time-format* 0)
      (format nil "~a" (uiop:run-program "date \"+%I:%M %p\"" :output '(:string :stripped t)))
      (if (eq *time-format* 1)
          (format nil "~a" (uiop:run-program "date \"+%Y %b %d (%a) $icon%I:%M %p\"" :output '(:string :stripped t))))))

(defun bot/modeline/date-and-time (ml)
  (declare (ignore ml))
  ;; (setf ml-str (bot/modeline/date-and-time *time-format*))
  (setf ml-str (bot/modeline/date-and-time/time))
  (format-with-on-click-id ml-str :bot/modeline/id/date-and-time/on-click nil))

(add-screen-mode-line-formatter #\T 'bot/modeline/date-and-time)

(defun bot/modeline/date-and-time/on-click (code id &rest rest)
  (declare (ignore rest))
  (declare (ignore id))
  (let ((button (stumpwm::decode-button-code code)))
    (case button
      ((:left-button)
       (defparameter *time-format* (mod (+ 1 *time-format*) 2))))
      (stumpwm::update-all-mode-lines)))

(register-ml-on-click-id :bot/modeline/id/date-and-time/on-click #'bot/modeline/date-and-time/on-click)

(defun bot/modeline/internet/get-network ()
  "Returns the name of the connected network"
  (setf *network-name* (uiop:run-program "iwgetid -r" :output '(:string :stripped t)))
  (if (= (length *network-name*) 0) ; checking if the string is empy meaning that no network is connected
      "Disconnected"
      *network-name*))

(defun bot/modeline/internet/open-settings ()
  "Opens nmtui"
  (uiop:launch-program "st -e nmtui"))

(defun bot/modeline/internet (ml)
  (declare (ignore ml))
  (setf ml-str (bot/modeline/internet/get-network))
  (format-with-on-click-id ml-str :bot/modeline/id/internet/on-click nil))

(add-screen-mode-line-formatter #\N 'bot/modeline/internet)

(defun bot/modeline/internet/on-click (code id &rest rest)
  (declare (ignore rest))
  (declare (ignore id))
  (let ((button (stumpwm::decode-button-code code)))
    (case button
      ((:left-button)
       (bot/modeline/internet/open-settings))
      (stumpwm::update-all-mode-lines))))

(register-ml-on-click-id :bot/modeline/id/internet/on-click #'bot/modeline/internet/on-click)

(defun bot/modeline/brightness/get-brightness ()
    "Returns the name of the connected wifi"
    (print (uiop:run-program "brightnessctl -d intel_backlight | grep Current | cut -d \' \' -f 4 | cut -d \'(\' -f 2 | cut -d \')\' -f 1" :output '(:string :stripped t))))

(setf *window-format* "%m%n%s%c")
(setf *time-modeline-string* "%Y %b %d (%a) %I:%M %p")
(setf cpu::*cpu-modeline-fmt* "%c %t")

 (setf *screen-mode-line-format*
          (list "[^B%n^b]" ; Group
            " %W"      ; Window
            "^>"       ; Push right
            " | %C"       ; cpu load
            " | Brightness: " '(:eval (bot/modeline/brightness/get-brightness))
            " | %N" ; internet
            " | %B"    ; Battery percentage
            " | %T" ; date and time
            )
          )

(dolist (h (screen-heads (current-screen)))   
  (enable-mode-line (current-screen) h t))

(defcommand bot/toggle-modeline () ()
    (stumpwm:toggle-mode-line (stumpwm:current-screen)
     (stumpwm:current-head))
  )

(defun key-press-hook (key key-seq cmd)
  (declare (ignore key))
  (unless (eq *top-map* *resize-map*)
    (let ((*message-window-gravity* :bottom-right))
      (message "Keys: ~a" (print-key-seq (reverse key-seq))))
    (when (stringp cmd)
      ;; give 'em time to read it
      (sleep 0.3))))

(defmacro replace-hook (hook fn)
  `(remove-hook ,hook ,fn)
  `(add-hook ,hook ,fn))

(replace-hook *key-press-hook* 'key-press-hook)
